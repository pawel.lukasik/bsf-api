package com.bsf.api.transfer;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.SpyBean;

import com.bsf.api.account.AccountRepository;
import com.bsf.api.common.AccountNotFoundException;
import com.bsf.api.model.Account;

@SpringBootTest
class LockableTransferServiceTest {

    private final BigDecimal FINAL_BALANCE = new BigDecimal("25.00");
    @Autowired
    private LockableTransferService lockableTransferService;

    @Autowired
    private AccountRepository accountRepository;

    @SpyBean
    private TransferService transferService;

    private final List<BigDecimal> increments25 = Arrays.asList(
            new BigDecimal(10),
            new BigDecimal(15)
    );

    private final List<BigDecimal> increments100 = Arrays.asList(
            new BigDecimal(10),
            new BigDecimal(15),
            new BigDecimal(25),
            new BigDecimal(50)
    );

    @Test
    void shouldAcceptTransfer() throws AccountNotFoundException {
        //given
        var inAccount = Account.builder()
                .number("100")
                .balance(new BigDecimal("0"))
                .build();
        final Account account = accountRepository.save(inAccount);
        assertEquals(0L, account.getVersion());


        //when
        for (var increment : increments25) {
            var transfer = TransferDTO.builder()
                    .accountNumber(inAccount.getNumber())
                    .amountOfMoney(increment)
                    .build();
            lockableTransferService.acceptTransfer(transfer);
        }

        //then
        var endAccount = accountRepository.findByNumber(inAccount.getNumber()).orElseThrow(() -> new NoSuchElementException("No item found"));

        assertAll(
                () -> assertEquals(2L, endAccount.getVersion()),
                () -> assertEquals(FINAL_BALANCE, endAccount.getBalance()),
                () -> verify(transferService, times(2)).acceptTransfer(any(TransferDTO.class))
        );
    }

    @Test
    void shouldAcceptTransfer_withSimplifiedConcurrentHandling() throws InterruptedException {
        //given
        var inAccount = Account.builder()
                .number("101")
                .balance(new BigDecimal("0"))
                .build();
        final Account account = accountRepository.save(inAccount);
        assertEquals(0L, account.getVersion());

        var executor = Executors.newFixedThreadPool(increments100.size());

        //when
        for (var increment : increments100) {
            executor.execute( () -> {
                var transfer = TransferDTO.builder()
                        .accountNumber(inAccount.getNumber())
                        .amountOfMoney(increment)
                        .build();
                try {
                    lockableTransferService.acceptTransfer(transfer);
                } catch (AccountNotFoundException e) {
                    throw new RuntimeException(e);
                }
            });
        }

        executor.shutdown();
        executor.awaitTermination(1, TimeUnit.MINUTES);

        //then
        var endAccount = accountRepository.findByNumber(inAccount.getNumber()).orElseThrow(() -> new NoSuchElementException("No item found"));

        assertAll(
                () -> assertEquals(increments100.size(), endAccount.getVersion()),
                () -> assertEquals(new BigDecimal("100.00"), endAccount.getBalance()),
                () -> verify(transferService, times(10)).acceptTransfer(any(TransferDTO.class))
        );
    }
}
