package com.bsf.api.transfer;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.math.BigDecimal;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.http.HttpStatus;

import com.bsf.api.account.AccountRepository;

@SpringBootTest
class TransferControllerTest {

    @Autowired
    private TransferController transferController;

    @SpyBean
    private AccountRepository accountRepository;

    @Test
    void creation() {
        assertThat(transferController).isNotNull();
    }

    @Test
    void shouldFailForNotExistingAccountNumber() {
        //given
        var transfer = TransferDTO.builder()
                .accountNumber("wrong-number")
                .build();

        //when
        var response = transferController.transfer(transfer);

        //then
        assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
    }

    @Test
    void shouldChangeBalanceOnGivenAccountNumber() {
        //given
        var transfer = TransferDTO.builder()
                .accountNumber("1")
                .amountOfMoney(new BigDecimal("50"))
                .build();

        //when
        var response = transferController.transfer(transfer);

        //then
        assertEquals(HttpStatus.OK, response.getStatusCode());
        verify(accountRepository, times(1)).findByNumber("1");
    }
}
