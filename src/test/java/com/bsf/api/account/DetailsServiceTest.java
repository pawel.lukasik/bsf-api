package com.bsf.api.account;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.bsf.api.common.AccountNotFoundException;

@SpringBootTest
class DetailsServiceTest {

    @Autowired
    private DetailsService detailsService;

    @Autowired
    private AccountRepository accountRepository;

    @Test
    void shouldReturnValidDetails() throws AccountNotFoundException {
        //given
        String accountNumber = "1";
        //when
        var response = detailsService.fetch(accountNumber);

        //then
        assertThat(response.getAccountNumber()).isEqualTo(accountNumber);
        assertThat(response.getBalance()).isEqualTo("0.00");
    }

    @Test
    void shouldThrowException() {
        //given
        String accountNumber = "123";
        //when
        Exception exception = assertThrows(AccountNotFoundException.class, () -> detailsService.fetch(accountNumber));

        //then
        assertThat(exception).isNotNull();
        assertThat(exception.getMessage()).isNullOrEmpty();

    }
}
