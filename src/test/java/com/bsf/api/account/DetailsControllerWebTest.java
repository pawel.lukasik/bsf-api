package com.bsf.api.account;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.hamcrest.Matchers;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

@SpringBootTest
@AutoConfigureMockMvc
class DetailsControllerWebTest {

    @Autowired
    private MockMvc mockMvc;

    @Test
    void shouldReturnValidDetails() throws Exception {

        this.mockMvc.perform(get("/details/1"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.accountNumber", Matchers.is("1")))
                .andExpect(jsonPath("$.balance", Matchers.is("0.00")));
    }

    @Test
    void shouldFailWith404() throws Exception {

        this.mockMvc.perform(get("/details/404-test"))
                .andExpect(status().is4xxClientError());
    }

}
