package com.bsf.api.account;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;

@SpringBootTest
class DetailsControllerTest {

    @Autowired
    private DetailsController detailsController;

    @Test
    void creation() {
        assertThat(detailsController).isNotNull();
    }

    @Test
    void shouldReturnValidDetails() {
        var response = detailsController.details("1");

        var details = DetailsDTO.builder()
                .accountNumber("1")
                .balance("0.00")
                .build();

        assertThat(response).isNotNull();
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(response.getBody()).isNotNull();
        assertThat(response.getBody()).isEqualTo(details);
    }

}
