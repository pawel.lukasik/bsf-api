package com.bsf.api.transfer;

import java.math.BigDecimal;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;

import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
@JsonPOJOBuilder
class TransferDTO {
    private final String description;
    @NotBlank
    private final String accountNumber;
    @Min(0)
    private final BigDecimal amountOfMoney;

}
