package com.bsf.api.transfer;

import org.springframework.orm.ObjectOptimisticLockingFailureException;
import org.springframework.stereotype.Service;

import com.bsf.api.common.AccountNotFoundException;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Service
@RequiredArgsConstructor
@Slf4j
class LockableTransferService {
    private final TransferService transferService;
    public void acceptTransfer(TransferDTO transfer) throws AccountNotFoundException {
        try {
            transferService.acceptTransfer(transfer);
        } catch (ObjectOptimisticLockingFailureException e) {
            log.warn("Somebody has already updated the account for transfer: {} in concurrent transaction. Will try again...", transfer);
            this.acceptTransfer(transfer);
        }
    }
}
