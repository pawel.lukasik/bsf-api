package com.bsf.api.transfer;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.bsf.api.account.AccountRepository;
import com.bsf.api.common.AccountNotFoundException;

import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
class TransferService {

    private final AccountRepository accountRepository;

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public void acceptTransfer(TransferDTO transfer) throws AccountNotFoundException {

        var account = accountRepository.findByNumber(transfer.getAccountNumber())
                .orElseThrow(AccountNotFoundException::new);
        account.setBalance(account.getBalance().add(transfer.getAmountOfMoney()));

        accountRepository.save(account);
    }
}
