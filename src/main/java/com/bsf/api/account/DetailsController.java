package com.bsf.api.account;

import javax.validation.Valid;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.bsf.api.common.AccountNotFoundException;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@RestController
@RequestMapping(produces = MediaType.APPLICATION_JSON_VALUE)
@RequiredArgsConstructor
@Slf4j
class DetailsController {

    private final DetailsService detailsService;

    @GetMapping(value = "/details/{account-number}")
    @ResponseStatus(code = HttpStatus.OK)
    @Operation(summary = "Getting the account details.",
            responses = {
                    @ApiResponse(responseCode = "200", description = "Success", content = @Content(mediaType = "application/json", schema = @Schema(implementation =  DetailsDTO.class))),
                    @ApiResponse(responseCode = "404", description = "Not found"),
            })
    public ResponseEntity<DetailsDTO> details(@Valid @PathVariable("account-number") String accountNumber) {
        try {
            var details = detailsService.fetch(accountNumber);
            return ResponseEntity.ok(details);
        } catch (AccountNotFoundException e) {
            return ResponseEntity.notFound().build();
        }

    }

}
