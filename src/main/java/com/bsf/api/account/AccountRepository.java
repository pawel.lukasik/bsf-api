package com.bsf.api.account;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.bsf.api.model.Account;

public interface AccountRepository extends JpaRepository<Account, Long> {

    Optional<Account> findByNumber(String number);
}
