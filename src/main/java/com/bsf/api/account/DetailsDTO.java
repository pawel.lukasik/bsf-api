package com.bsf.api.account;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
@AllArgsConstructor
class DetailsDTO {
    private final String accountNumber;
    private final String balance;

}
