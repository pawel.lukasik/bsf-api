package com.bsf.api.account;

import org.springframework.stereotype.Service;

import com.bsf.api.common.AccountNotFoundException;

import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
class DetailsService {

    private final AccountRepository accountRepository;
    public DetailsDTO fetch(String accountNumber) throws AccountNotFoundException {

        var account = accountRepository.findByNumber(accountNumber).orElseThrow(AccountNotFoundException::new);
        
        return DetailsDTO.builder()
                .accountNumber(account.getNumber())
                .balance(account.getBalance().toPlainString())
                .build();
    }

}
